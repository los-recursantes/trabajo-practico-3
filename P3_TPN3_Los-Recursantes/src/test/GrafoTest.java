package test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



import negocio.Grafo;

class GrafoTest {
	
	@Test
	void TieneAritstasTest()
	{
		Grafo g=new Grafo();
		g.agregarVertice(3);
		g.agregarVertice(5);
		g.agregarArista(0, 1);
		assertEquals(true , g.tieneAristas());
	}
	
	@Test
	void tamanoGrafoNormalTest() {
		Grafo g= new Grafo();
		g.agregarVertice(7);
		g.agregarVertice(2);
		g.agregarVertice(4);
		assertEquals(3, g.tamano());
	}
	
	@Test
	void pesoGrafoTest() {
		Grafo g=new Grafo();
		g.agregarVertice(7);
		g.agregarVertice(8);
		g.agregarVertice(2);
		assertEquals(17, g.pesoGrafo());
	}
	
	@Test
	void tamanoGrafoUnVerticeTest() {
		Grafo g= new Grafo();
		g.agregarVertice(5);
		assertEquals(1,g.tamano());
	}
	
	@Test
	void pesoVerticeTest() {
		Grafo g= new Grafo();
		g.agregarVertice(5);
		g.agregarVertice(9);
		assertEquals(5,g.pesoVertice(0));
	}
	
	@Test
	void eliminarVerticeNuloTest() {
		Grafo g=new Grafo();
		Assertions.assertThrows(IllegalArgumentException.class, ()->{
			g.eliminarVertice(0);
		});
	}
	
	@Test
	void eliminarVerticeTest() {
		Grafo g= new Grafo();
		g.agregarVertice(4);
		g.agregarVertice(3);
		g.eliminarVertice(0);
		assertEquals(3,g.pesoGrafo());
	}
	@Test
	void AgregarAristaTest()
	{
		Grafo g=new Grafo();
		g.agregarVertice(22);
		g.agregarVertice(5);
		g.agregarVertice(5);
		g.agregarVertice(23);
		g.agregarArista(1, 3);
		assertEquals(false, g.existeArista(0, 3));
	}	
	
	@Test
	void EliminarAristaTest()
	{
		Grafo g=new Grafo();
		g.agregarVertice(22);
		g.agregarVertice(5);
		g.agregarVertice(5);
		g.agregarVertice(23);
		g.agregarArista(0, 3);
		g.agregarArista(0, 2);
		g.agregarArista(1, 3);
		g.eliminarArista(0, 2);
		assertEquals(false, g.existeArista(0, 2));
	}

	@Test
	public void AristaInexistenteTest()
	{
		Grafo g=new Grafo();
		g.agregarVertice(7);
		g.agregarVertice(5);
		g.agregarVertice(9);
		
		Assertions.assertThrows(IllegalArgumentException.class, ()->{
			g.agregarArista(0, 4);	
		});
	}
	
	
	
	
}
