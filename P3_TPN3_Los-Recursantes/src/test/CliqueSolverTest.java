package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import negocio.CliqueSolver;
import negocio.ComparadorPorPeso;
import negocio.Grafo;

public class CliqueSolverTest {
	
	
	@Test
	public 	void ResolverTest()
	{
		Grafo g=new Grafo();
		g.agregarVertice(27);
		g.agregarVertice(5);
		g.agregarVertice(9);
		g.agregarVertice(8);
		g.agregarArista(0, 3);
		g.agregarArista(1, 2);
		g.agregarArista(0, 1);
		g.agregarArista(3, 2);
		CliqueSolver solver=new CliqueSolver(g, new ComparadorPorPeso());
		Grafo clique=new Grafo();
		clique=solver.resolver();
		assertEquals(35, clique.pesoGrafo());
			
	}
	

}
