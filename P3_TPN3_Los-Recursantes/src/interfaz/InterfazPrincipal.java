package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

import negocio.CliqueSolver;
import negocio.ComparadorPorPeso;
import negocio.Grafo;

import java.awt.Color;

public class InterfazPrincipal {

	private static InterfazPrincipal interfaz;
	private InterfazVerGrafo verGrafo, verResultado;
	private Grafo grafo;
	private JFrame frmCliquesGolosas;
	private JPanel panelTitulo, panelBotones;
	private JLabel lblTitulo, lblTitulo2;
	private JButton btnVerGrafo, btnMostrarSolucion;
	private JButton btnRegistrarDatos;
	
	//Ejecuta la ventana
	public static void ejecutar() {
		
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		}
		catch(Exception E) {
			E.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					interfaz = new InterfazPrincipal();
					interfaz.frmCliquesGolosas.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Constructor
	public InterfazPrincipal() {
		initialize();
		inicializarGrafo();
		inicializarInterfaces();
	}
	
	//Inicializa los elementos de la ventana
	private void initialize() {
		inicializarFrame();
		inicializarPaneles();
		inicializarLabels();
		inicializarBotones();
	}

	//Inicializa el frame
	private void inicializarFrame() {
		frmCliquesGolosas = new JFrame();
		frmCliquesGolosas.setTitle("Cliques golosas");
		frmCliquesGolosas.setResizable(false);
		frmCliquesGolosas.setBounds(100, 100, 625, 208);
		frmCliquesGolosas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCliquesGolosas.getContentPane().setLayout(null);
	}
	
	//Inicializa los paneles
	private void inicializarPaneles() {
		panelBotones = new JPanel();
		panelBotones.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelBotones.setBounds(309, 13, 298, 148);
		frmCliquesGolosas.getContentPane().add(panelBotones);
		panelBotones.setLayout(null);
		
		panelTitulo = new JPanel();
		panelTitulo.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelTitulo.setBounds(12, 13, 288, 148);
		frmCliquesGolosas.getContentPane().add(panelTitulo);
		panelTitulo.setLayout(null);
	}
	
	//Inicializa los labels
	private void inicializarLabels() {
		lblTitulo = new JLabel("Cliques");
		lblTitulo.setFont(new Font("Constantia", Font.PLAIN, 51));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(12, 6, 264, 86);
		panelTitulo.add(lblTitulo);
		
		lblTitulo2 = new JLabel("golosas");
		lblTitulo2.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo2.setFont(new Font("Constantia", Font.PLAIN, 51));
		lblTitulo2.setBounds(12, 73, 264, 69);
		panelTitulo.add(lblTitulo2);
	}
	
	//Inicializa los botones
	private void inicializarBotones() {
		//Boton ver grafo
		btnVerGrafo = new JButton("Ver grafo");
		btnVerGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrarGrafo();
				
			};
		});
		btnVerGrafo.setBounds(12, 54, 274, 36);
		panelBotones.add(btnVerGrafo);
		
		//Boton mostrar solucion
		btnMostrarSolucion = new JButton("Mostrar solucion");
		btnMostrarSolucion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrarResultado();
			}
		});
		btnMostrarSolucion.setBounds(12, 102, 274, 36);
		panelBotones.add(btnMostrarSolucion);
		
		//Boton datos
		btnRegistrarDatos = new JButton("Datos");
		btnRegistrarDatos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InterfazRegistrarDatos.getInterfaz().mostrar();
			}
		});
		btnRegistrarDatos.setBounds(12, 6, 274, 36);
		panelBotones.add(btnRegistrarDatos);
	}
	
	//Inicializa el resto de ventanas
	private void inicializarInterfaces() {
		InterfazRegistrarDatos.ejecutar();
		
		verGrafo= new InterfazVerGrafo();
		verResultado= new InterfazVerGrafo();
		
		verGrafo.ejecutar();
		verResultado.ejecutar();
	}
	
	//Vuelve a crear la ventana de resultado
	private void actualizarResultado() {
		verResultado= new InterfazVerGrafo();
		verResultado.ejecutar();
	}
	
	//Inicializa el grafo
	private void inicializarGrafo() {
		grafo=new Grafo();
	}
	
	//Devuelve el grafo
	public Grafo getGrafo() {
		return grafo;
	}	
	
	//Devuelve la interfaz
	public static InterfazPrincipal getInterfaz() {
		return InterfazPrincipal.interfaz;
	}
	
	//Funcion asignada al boton mostrar grafo
	public void mostrarGrafo() {
		verGrafo.cargarGrafo(grafo);
		verGrafo.mostrar();
	}
	
	//Funcion asignada al boton mostrar resultado
	public void mostrarResultado() {
		try {
			CliqueSolver solver= new CliqueSolver(grafo, new ComparadorPorPeso());
			Grafo clique = solver.resolver();
			actualizarResultado();
			verResultado.cargarGrafo(clique);
			verResultado.mostrar();
		}
		catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Debe existir al menos 1 arista");
		}	
	}
	
}
