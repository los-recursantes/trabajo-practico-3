package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import negocio.Grafo;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazRegistrarDatos {

	private static InterfazRegistrarDatos interfaz;
	private JFrame frmRegistrar;
	private JPanel panelTablaVertices, panelTablaAristas, panelRegistroVertices, panelRegistroAristas;
	private JTable tableVertices, tableAristas;
	private DefaultTableModel tableModelVertices, tableModelAristas;
	private JScrollPane scrollVertices, scrollAristas;
	private JTextField textFieldValorVertice, textFieldVerticeA, textFieldVerticeB, textFieldNumeroVertice;
	private JLabel lblRegistroVertices, lblRegistroAristas, lblValorVertice, lblVerticeA, lblVerticeB, lblNumeroVertice;
	private JButton btnRegistrarArista, btnRegistrarVertice, btnBorrarArista, btnBorrarVertice, btnAyuda;
	private Grafo grafo;

	//Ejecuta la ventana
	public static void ejecutar() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					interfaz = new InterfazRegistrarDatos();
					interfaz.frmRegistrar.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	//Constructor
	public InterfazRegistrarDatos() {
		initialize();
		cargarGrafo();
	}

	//Inicializa todos los elementos de la ventana
	private void initialize() {
		inicializarFrame();
		inicializarPaneles();
		inicializarTablas();
		inicializarScrolls();
		inicializarLabels();
		inicializarTextFields();
		inicializarBotones();
	}
	
	//Inicializa el frame
	private void inicializarFrame() {
		frmRegistrar = new JFrame();
		frmRegistrar.setTitle("Registrar");
		frmRegistrar.setResizable(false);
		frmRegistrar.setBounds(100, 100, 670, 547);
		frmRegistrar.getContentPane().setLayout(null);
	}
	
	//Inicializa los paneles
	private void inicializarPaneles() {
		panelTablaVertices = new JPanel();
		panelTablaVertices.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelTablaVertices.setBounds(12, 13, 314, 306);
		frmRegistrar.getContentPane().add(panelTablaVertices);
		panelTablaVertices.setLayout(null);
		
		panelTablaAristas = new JPanel();
		panelTablaAristas.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelTablaAristas.setBounds(338, 13, 314, 306);
		frmRegistrar.getContentPane().add(panelTablaAristas);
		panelTablaAristas.setLayout(null);
		
		panelRegistroVertices = new JPanel();
		panelRegistroVertices.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelRegistroVertices.setBounds(12, 332, 314, 167);
		frmRegistrar.getContentPane().add(panelRegistroVertices);
		panelRegistroVertices.setLayout(null);
		
		panelRegistroAristas = new JPanel();
		panelRegistroAristas.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelRegistroAristas.setBounds(338, 332, 314, 167);
		frmRegistrar.getContentPane().add(panelRegistroAristas);
		panelRegistroAristas.setLayout(null);
	}
	
	//Inicializa las tablas y sus modelos
	private void inicializarTablas() {
		tableVertices = new JTable();
		tableVertices.setBorder(new LineBorder(new Color(0, 0, 0)));
		tableVertices.setBounds(12, 13, 290, 280);
		panelTablaVertices.add(tableVertices);
		
		tableModelVertices= new DefaultTableModel();
		tableModelVertices.addColumn("Numero");
		tableModelVertices.addColumn("Valor");
	
		tableVertices.setModel(tableModelVertices);
	
		tableAristas = new JTable();
		tableAristas.setBorder(new LineBorder(new Color(0, 0, 0)));
		tableAristas.setBounds(12, 13, 290, 280);
		panelTablaAristas.add(tableAristas);
		
		tableModelAristas= new DefaultTableModel();
		tableModelAristas.addColumn("Vertice A");
		tableModelAristas.addColumn("Vertice B");
	
		tableAristas.setModel(tableModelAristas);
	}
	
	//Inicializa las scrollbars
	private void inicializarScrolls() {
		scrollVertices=new JScrollPane(tableVertices); 
		scrollVertices.setBounds(12, 13, 290, 280);
		panelTablaVertices.add(scrollVertices);
	
		scrollAristas=new JScrollPane(tableAristas); 
		scrollAristas.setBounds(12, 13, 290, 280);
		panelTablaAristas.add(scrollAristas);
	}
	
	//Inicializa los labels
	private void inicializarLabels() {
		lblRegistroVertices = new JLabel("Registro de Vertices");
		lblRegistroVertices.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblRegistroVertices.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistroVertices.setBounds(52, 5, 211, 31);
		panelRegistroVertices.add(lblRegistroVertices);
		
		lblValorVertice = new JLabel("Valor:");
		lblValorVertice.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblValorVertice.setBounds(31, 51, 55, 26);
		panelRegistroVertices.add(lblValorVertice);
		
		lblNumeroVertice = new JLabel("Numero:");
		lblNumeroVertice.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNumeroVertice.setBounds(23, 81, 80, 26);
		panelRegistroVertices.add(lblNumeroVertice);
		
		lblRegistroAristas = new JLabel("Registro de Aristas");
		lblRegistroAristas.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistroAristas.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblRegistroAristas.setBounds(52, 5, 211, 31);
		panelRegistroAristas.add(lblRegistroAristas);
		
		lblVerticeA = new JLabel("Vertice A:");
		lblVerticeA.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblVerticeA.setBounds(52, 51, 84, 26);
		panelRegistroAristas.add(lblVerticeA);
		
		lblVerticeB = new JLabel("Vertice B:");
		lblVerticeB.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblVerticeB.setBounds(52, 81, 84, 26);
		panelRegistroAristas.add(lblVerticeB);
	}
	
	//Inicializa los textFields
	private void inicializarTextFields() {
		textFieldValorVertice = new JTextField();
		textFieldValorVertice.setColumns(10);
		textFieldValorVertice.setBounds(90, 55, 201, 22);
		panelRegistroVertices.add(textFieldValorVertice);
		
		textFieldNumeroVertice = new JTextField();
		textFieldNumeroVertice.setColumns(10);
		textFieldNumeroVertice.setBounds(90, 85, 140, 22);
		panelRegistroVertices.add(textFieldNumeroVertice);
		
		textFieldVerticeA = new JTextField();
		textFieldVerticeA.setColumns(10);
		textFieldVerticeA.setBounds(148, 55, 116, 22);
		panelRegistroAristas.add(textFieldVerticeA);
		
		textFieldVerticeB = new JTextField();
		textFieldVerticeB.setColumns(10);
		textFieldVerticeB.setBounds(148, 85, 116, 22);
		panelRegistroAristas.add(textFieldVerticeB);
	}
	
	//Inicializa los botones
	private void inicializarBotones() {
		//Boton registrar vertice
		btnRegistrarVertice = new JButton("Registrar");
		btnRegistrarVertice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				registrarVertice();
			}
		});
		btnRegistrarVertice.setBounds(62, 123, 91, 31);
		panelRegistroVertices.add(btnRegistrarVertice);
		
		//Boton borrar vertice
		btnBorrarVertice = new JButton("Borrar");
		btnBorrarVertice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrarVertice();
			}
		});
		btnBorrarVertice.setBounds(165, 123, 91, 31);
		panelRegistroVertices.add(btnBorrarVertice);
		
		//Boton ayuda
		btnAyuda = new JButton("\u00BF?");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayuda();
			}
		});
		btnAyuda.setBounds(242, 84, 49, 25);
		panelRegistroVertices.add(btnAyuda);
		
		//Boton registrar arista
		btnRegistrarArista = new JButton("Registrar");
		btnRegistrarArista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				registrarArista();
			}
		});
		btnRegistrarArista.setBounds(62, 123, 91, 31);
		panelRegistroAristas.add(btnRegistrarArista);
		
		//Boton borrar arista
		btnBorrarArista = new JButton("Borrar");
		btnBorrarArista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrarArista();
			}
		});
		btnBorrarArista.setBounds(165, 123, 91, 31);
		panelRegistroAristas.add(btnBorrarArista);
	}
	
	// Devuelve la ventana, para poder ejecutarla desde otras partes del programa.
	public static InterfazRegistrarDatos getInterfaz() {
		return InterfazRegistrarDatos.interfaz;
	}
	
	// Hace visible la ventana
	public void mostrar() {
		interfaz.frmRegistrar.setVisible(true);
	}
	
	// Carga el grafo en esta ventana, para poder modificarlo desde aca
	public void cargarGrafo() {
		grafo= InterfazPrincipal.getInterfaz().getGrafo();
	}
	
	// Muestra una mensaje de ayuda
	private void ayuda() {
		JOptionPane.showMessageDialog(null,"El valor es necesario solo al registrar\nEl numero es necesario solo al eliminar");
	}
	
	// Registra un vertice en la tabla y en el grafo
	private void registrarVertice() {
		try {
			String pesoStr=textFieldValorVertice.getText();
			int peso=Integer.parseInt(pesoStr);
			int numero=tableModelVertices.getRowCount();
			String numeroStr=numero+"";
			
			grafo.agregarVertice(peso);
			tableModelVertices.addRow(new String[] {numeroStr,pesoStr});
			textFieldValorVertice.setText("");
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Ingrese un peso valido");
			textFieldValorVertice.setText("");
		}	
	}
	
	// Borra el vertice indicado en la tabla y en el grafo (Si hay aristas que lo incluyen, las borra tambien)
	private void borrarVertice() {
		try {
			String numeroStr= textFieldNumeroVertice.getText();
			int numero= Integer.parseInt(numeroStr);
			
			tableModelVertices.removeRow(numero);
			grafo.eliminarVertice(numero);
			eliminarVerticeAristas(numeroStr);
			reordenarNumerosVertices(); 
			reordenarNumerosAristas();
			textFieldNumeroVertice.setText("");
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Ingrese un numero de vertice correcto");
			textFieldNumeroVertice.setText("");
		}
	}
	
	// Registra una arista en la tabla y en el grafo
	private void registrarArista() {
		try {
			String verticeAStr=textFieldVerticeA.getText();
			String verticeBStr=textFieldVerticeB.getText();
			int verticeA=Integer.parseInt(verticeAStr);
			int verticeB=Integer.parseInt(verticeBStr);
			
			if(grafo.existeArista(verticeA, verticeB)) {
				JOptionPane.showMessageDialog(null, "La arista ya existe");
				limpiarTextFieldsAristas();
			}
			else {
				grafo.agregarArista(verticeA, verticeB);
				tableModelAristas.addRow(new String[] {verticeAStr,verticeBStr});
				limpiarTextFieldsAristas();
			}
		}
		catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Ingrese vertices validos");
			limpiarTextFieldsAristas();
		}
		
	}
	
	// Borra una arista de la tabla y el gafo
	private void borrarArista() {
		try {
			String verticeAStr=textFieldVerticeA.getText();
			String verticeBStr=textFieldVerticeB.getText();
			int verticeA=Integer.parseInt(verticeAStr);
			int verticeB=Integer.parseInt(verticeBStr);
			
			if(grafo.existeArista(verticeA, verticeB)) {
				grafo.eliminarArista(verticeA, verticeB);
				tableModelAristas.removeRow(devolverNumeroDeFilaAristas(verticeAStr, verticeBStr));
				limpiarTextFieldsAristas();
			}
			else {
				JOptionPane.showMessageDialog(null, "La arista no existe");
				limpiarTextFieldsAristas();
			}
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Ingrese vertices validos");
			limpiarTextFieldsAristas();
		}
	}

	// Devuelve el numero de fila de la tabla aristas en la que esta registrada la arista que conecta v1 y v2
	private int devolverNumeroDeFilaAristas(String v1, String v2) {
		int filas=tableModelAristas.getRowCount()-1;
		for(int i=filas;i>=0;i--) {
			if(tableModelAristas.getValueAt(i, 0).equals(v1) && tableModelAristas.getValueAt(i, 1).equals(v2)){
				return i;
			}
			else if(tableModelAristas.getValueAt(i,0).equals(v2) && tableModelAristas.getValueAt(i,1).equals(v1)) {
				return i;
			}
		}
		return 0;
	}
	
	// Elimina todas las aristas que contengan el vertice indicado
	private void eliminarVerticeAristas(String numVertice) {
		int filas=tableModelAristas.getRowCount()-1;
		for(int i=filas;i>=0;i--) {
			if(tableModelAristas.getValueAt(i, 0).equals(numVertice) || tableModelAristas.getValueAt(i, 1).equals(numVertice)){
				tableModelAristas.removeRow(i);
			}
		}
	}
	
	// Renombra los vertices en la tabla, asignandoles el numero de fila como nombre
	private void reordenarNumerosVertices() {
		int filas=tableModelVertices.getRowCount()-1;
		for(int i=filas;i>=0;i--) {
			tableModelVertices.setValueAt(i, i, 0);
		}
	}
	
	// Renombra los vertices de la tabla de aristas
	private void reordenarNumerosAristas() {
		int filas=tableModelAristas.getRowCount();
		for(int i=0;i<filas;i++) {
			int verticeA= Integer.parseInt((String)tableModelAristas.getValueAt(i,0))-1;
			int verticeB= Integer.parseInt((String)tableModelAristas.getValueAt(i,1))-1;
			
			if(Integer.parseInt((String)tableModelAristas.getValueAt(i, 0))>=Integer.parseInt(textFieldNumeroVertice.getText())) {
				tableModelAristas.setValueAt(verticeA+"", i, 0);
			}
			
			if(Integer.parseInt((String)tableModelAristas.getValueAt(i, 1))>=Integer.parseInt(textFieldNumeroVertice.getText())) {
				tableModelAristas.setValueAt(verticeB+"", i, 1);
			}
			
		}
	}
	
	// Borra los textfields del registro de aristas
	private void limpiarTextFieldsAristas() {
		textFieldVerticeA.setText("");
		textFieldVerticeB.setText("");
	}
	
}
