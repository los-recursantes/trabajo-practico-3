package interfaz;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import negocio.Grafo;
import negocio.Vertice;

public class PanelGrafo extends JPanel {

	private static final long serialVersionUID = 1L;
	private Map<Integer,Point> puntos;
	private ArrayList<Vertice> vertices;
	private Grafo grafo;
	
	PanelGrafo(Grafo g){
		this.setBackground(Color.darkGray);
		this.grafo=g;
	}
	
	@Override
	public void paint(Graphics g){
		if(grafo!=null) {
			super.paint(g);
			puntos =  new HashMap<Integer,Point>();
			vertices=grafo.getVertices();
			int x=20, x2=60, x3=60;
			int y=120,y2=30,y3=210;
			int cont=0;
			boolean b=true;
			for(int i=0;i<vertices.size();i++){
				if(cont==0){
					puntos.put(vertices.get(i).getNumero(),new Point(x, y));
					x=x+80;
					b=true;}
				if(cont==1){
					puntos.put(vertices.get(i).getNumero(),new Point(x2, y2));
					x2=x2+80;
					b=true;}
				if(cont==2){
					puntos.put(vertices.get(i).getNumero(),new Point(x3, y3));
					x3=x3+80;
					cont=0;
					b=false;}
				if(b==true){
					cont++;}
			}
			//Dibuja las lineas
			g.setColor(Color.BLUE);
			for(int j=0;j<vertices.size();j++){
				int num=vertices.get(j).getNumero();
				int[] vecinos=vertices.get(j).arrayVecinos();
				for(int k=0;k<vecinos.length;k++){
					g.drawLine(puntos.get(num).x+15,puntos.get(num).y+15,puntos.get(vecinos[k]).x+15,puntos.get(vecinos[k]).y+15);
				}
			}
			//dibuja los vertices
			g.setColor(Color.CYAN);
			puntos.forEach((k,v)-> g.fillOval((int)v.getX(),(int)v.getY(), 30, 30));

			//Dibuja los numeros	
			g.setColor(Color.BLACK);	
			for(int z=0;z<vertices.size();z++) {
				int numero=vertices.get(z).getNumero();
				g.drawString(String.valueOf(numero), puntos.get(numero).x+10, puntos.get(numero).y+20);
			}
		}
	}
	
	public  void mostrar() {
		this.setVisible(true);
	}
	public void setGrafo(Grafo g) {
		grafo= g;
	}
		
		
}
