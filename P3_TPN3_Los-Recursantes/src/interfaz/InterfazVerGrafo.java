package interfaz;

import java.awt.EventQueue;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;

import negocio.Grafo;


public class InterfazVerGrafo {

	private JFrame frmGrafo;
	private JPanel panelMensajes;
	private JLabel lblCantidadVertices, lblPesoTotal;
	private Grafo grafo;
	private PanelGrafo panelGrafo;

	//Ejecuta la ventana
	public void ejecutar() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazVerGrafo window = new InterfazVerGrafo();
					window.frmGrafo.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Constructor
	public InterfazVerGrafo() {
		initialize();
	}

	//Inicializa todos los elementos de la ventana
	private void initialize() {
		inicializarFrame();
		inicializarPanel();
		inicializarLabels();
	}

	//Inicializa el frame
	private void inicializarFrame() {
		frmGrafo = new JFrame();
		frmGrafo.setTitle("Grafo");
		frmGrafo.setResizable(false);
		frmGrafo.setBounds(100, 100, 520, 399);
		frmGrafo.getContentPane().setLayout(null);
	}
	
	//Inicializa el panel de mensajes
	private void inicializarPanel() {
		panelMensajes = new JPanel();
		panelMensajes.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelMensajes.setBounds(12, 298, 490, 53);
		frmGrafo.getContentPane().add(panelMensajes);
		panelMensajes.setLayout(null);
	}
	
	//Inicializa los labels
	private void inicializarLabels() {
		lblPesoTotal = new JLabel("Peso total:");
		lblPesoTotal.setBounds(12, 13, 182, 27);
		panelMensajes.add(lblPesoTotal);
		
		lblCantidadVertices = new JLabel("Cantidad de vertices:");
		lblCantidadVertices.setBounds(259, 13, 219, 27);
		panelMensajes.add(lblCantidadVertices);
	}
	
	//Crea el panel donde se dibuja el grafo (y lo dibuja)
	private void dibujarGrafo() {
		panelGrafo = new PanelGrafo(grafo);
		panelGrafo.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelGrafo.setBounds(12, 13, 490, 272);
		frmGrafo.getContentPane().add(panelGrafo);
	}
	
	//Muestra la ventana
	public void mostrar() {
		dibujarGrafo();
		actualizarLabels();
		this.frmGrafo.setVisible(true);
	}
	
	//Carga el grafo
	public void cargarGrafo(Grafo grafo) {
		this.grafo=grafo;
	}
	
	//Actualiza los labels con los nuevos valores
	public void actualizarLabels() {
		lblCantidadVertices.setText("Cantidad de vertices: "+grafo.tamano());
		lblPesoTotal.setText("Peso total: "+grafo.pesoGrafo());
	}
}