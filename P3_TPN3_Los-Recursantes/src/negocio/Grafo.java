package negocio;

import java.util.ArrayList;
import java.util.Set;

public class Grafo {
	// Representamos el grafo con listas de vecinos
	private ArrayList<Vertice> listaVertices;

	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo() {
		this.listaVertices = new ArrayList<Vertice>();
	}
	
	// Indica si el grafo tiene al menos 1 arista
	public boolean tieneAristas() {
		for(Vertice v: listaVertices) {
			if (v.cantidadVecinos()>0) {
				return true;
			}
		}
		return false;
	}
	
	// Agrega un vertice al grafo
	public void agregarVertice(int peso) {
		listaVertices.add(new Vertice(peso,listaVertices.size()));
	}
	
	// Agrega un vertice al grafo, con un numero ya asignado (Utilizado solo para crear el grafo de la clique)
	void agregarVertice(int peso, int numero) {
		listaVertices.add(new Vertice(peso,numero));
	}
	
	// Elimina un vertice del grafo
	public void eliminarVertice(int vertice) {
		verificarVertice(vertice);
		for(int i=0;i<listaVertices.size();i++) if(i!=vertice) {
			vertice(i).eliminarVecino(vertice(vertice));
		}
		listaVertices.remove(vertice);
		renombrarVertices();
	}
	
	// Le asigna a cada vertice el numero correspondiente a su posicion en la lista
	public void renombrarVertices() {
		for(int i=0;i<listaVertices.size();i++) {
			vertice(i).setNumero(i);
		}
	}
	
	// Devuelve el vertice numero i
	private Vertice vertice(int i) {
		return listaVertices.get(i);
	}
	
	// Agregado de aristas
	public void agregarArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
		vertice(i).agregarVecino(vertice(j));
		vertice(j).agregarVecino(vertice(i));
	}

	// Eliminacion de aristas
	public void eliminarArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		vertice(i).eliminarVecino(vertice(j));
		vertice(j).eliminarVecino(vertice(i));
	}

	// Informa si existe la arista especificada
	public boolean existeArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return vertice(i).getVecinos().contains(vertice(j));
	}
	
	// Cantidad de vertices
	public  int tamano() {
		return listaVertices.size();
	}

	// Devuelve el peso del vertice indicado
	public int pesoVertice(int vertice) {
		verificarVertice(vertice);
		return vertice(vertice).getPeso();
	}
	
	// Devuelve un set con los vecinos del vertice indicado
	public Set<Vertice> vecinos(int vertice) {
		verificarVertice(vertice);
		return vertice(vertice).getVecinos();
	}

	// Devuelve el peso total del grafo (suma del peso de los vertices)
	public int pesoGrafo() {
		int suma=0;
		for(Vertice v : listaVertices) {
			suma+=v.getPeso();
		}
		return suma;
	}
	
	// Devuelve una copia de la lista de vertices (Para cuando se tiene que trabajar sobre ella, para no modificar la original)
	public ArrayList<Vertice> clonarListaVertices(){
		ArrayList<Vertice> copia= new ArrayList<Vertice>();

		if(this.tamano()>0) {
			for(int i=0;i<listaVertices.size();i++) {
				copia.add(vertice(i).clonarVertice());							// Crea copias de los vertices en una nueva lista.
			}
			
			for(int i=0;i<listaVertices.size();i++) {
				for(Vertice vecino: vertice(i).getVecinos()) {
					copia.get(i).agregarVecino(copia.get(vecino.getNumero()));	// Se fija que vecinos tiene el vertice i original,
				}																// 	 y toma sus numeros. Al vertice copia i le asigna 
			}																	// 	 como vecino los vertices correspondientes al numero 														//	 indicado en los vertices originales. 
		}
		return copia;
	}
		
	// Devuelve la lista de vertices (Para cuando no tiene que ser modificada la lista, sino que solo se recorren sus valores)
	public ArrayList<Vertice> getVertices(){
		return listaVertices;
	}
		
	// Imprime los vertices y sus vecinos.
	public void imprimirListaVecinos() {
		for(Vertice v : listaVertices) {
			System.out.print("\nVertice: "+v.getNumero()+" -> Vecinos: ");
			int[] vecinos= v.arrayVecinos();
			for(int numero : vecinos) {
				System.out.print(numero+"  ");
			}
		}
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i) {
		if (i < 0)
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);

		if (i >= tamano())
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j) {
		if (i == j)
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}

}
