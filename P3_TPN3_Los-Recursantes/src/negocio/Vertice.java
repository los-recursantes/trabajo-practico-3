package negocio;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Vertice {
	private int peso;
	private Set<Vertice> vecinos;
	private int numero;
	
	//Constructor
	public Vertice(int peso, int numero) {
		if(peso<0 || numero<0) {
			throw new RuntimeException("El vertice no puede tener numero o peso negativo");
		}
		this.peso=peso;
		this.numero=numero;
		this.vecinos=new HashSet<Vertice>();
	}
	
	//Devuelve el peso
	public int getPeso() {
		return this.peso;
	}
	
	//Setea el numero
	public void setNumero(int numero) {
		this.numero=numero;
	}
	
	//Devuelve el numero
	public int getNumero() {
		return this.numero;
	}
	
	//Devuelve un set con los vecinos
	public Set<Vertice> getVecinos() {
		return this.vecinos;
	}
	
	//Devuelve un array con los numeros de los vecinos
	public int[] arrayVecinos () {
		int[] vecinos= new int[this.cantidadVecinos()];
		int contador=0;
		Iterator<Vertice> it= this.getVecinos().iterator();
		while(it.hasNext()) {
			vecinos[contador]=it.next().getNumero();
			contador++;
		}
		return vecinos;
	}
	
	//Agrega un vecino al vertice
	public void agregarVecino(Vertice vertice) {
		vecinos.add(vertice);
	}
	
	//Elimina un vecino al vertice
	public void eliminarVecino(Vertice vertice) {
		if(vecinos.contains(vertice)) {
			vecinos.remove(vertice);
		}
	}
	
	//Devuelve la cantidad de vecinos del vertice
	public int cantidadVecinos() {
		return vecinos.size();
	}
	
	//Se fija si el vertice es vecino del vertice v.
	public boolean esVecino(Vertice v) {
		return this.vecinos.contains(v);
	}
	
	// Crea un vertice con el mismo peso y numero que el actual.
	public Vertice clonarVertice() {
		return new Vertice(this.peso, this.numero);
	}
	
}
