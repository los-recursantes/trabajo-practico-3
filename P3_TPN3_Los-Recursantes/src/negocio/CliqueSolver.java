package negocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CliqueSolver {
	private Grafo grafo;
	private Comparator<Vertice> comparador;
	
	// Constructor
	public CliqueSolver(Grafo grafo, Comparator<Vertice> comparador) {
		if(grafo.tamano()>0 && grafo.tieneAristas()) {
			this.grafo=grafo;
			this.comparador=comparador;
		}
		else {
			throw new IllegalArgumentException("El grafo tiene que tener al menos 1 arista");
		}
	}
	
	// Crea un grafo con los vertices que cumplen las condiciones y lo devuelve.
	public Grafo resolver() {
		Grafo g=new Grafo();
		
		ArrayList<Vertice> vertices=obtenerVerticesCompatibles();
		for(Vertice v : vertices) {
			g.agregarVertice(v.getPeso(),v.getNumero());
		}
		
		for(int i=0;i<g.tamano();i++) {
			for(int j=0;j<g.tamano();j++) {
				if(i!=j && !g.existeArista(i, j)) {
					g.agregarArista(i, j);
				}
			}
		}
		return g;
	}

	// Devuelve los vertices que cumplen las condiciones para ser una posible mayor clique.
	private ArrayList<Vertice> obtenerVerticesCompatibles() {
		ArrayList<Vertice> verticesOrdenados = ordenarVertices();
		ArrayList<Vertice> marcados= new ArrayList<Vertice>();
		
		if(marcados.size()==0) {
			for(Vertice v: verticesOrdenados) {
				if(v.cantidadVecinos()>0) {
					marcados.add(v);
					break;
				}
			}
		}
		
		for(Vertice v : verticesOrdenados) {
			boolean valido=true;
			
			for(Vertice v2: marcados) {
				valido=valido && v.esVecino(v2);
			}
			
			if(valido) {
				marcados.add(v);
			}
		}
		return marcados;
	}
	
	// Devuelve una copia ordenada de los vertices del grafo.
	private ArrayList<Vertice> ordenarVertices() {
		ArrayList<Vertice> listaVertices=grafo.clonarListaVertices();
		Collections.sort(listaVertices, comparador);
		
		return listaVertices;
	}
}
