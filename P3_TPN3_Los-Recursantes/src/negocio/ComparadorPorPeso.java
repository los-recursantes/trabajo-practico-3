package negocio;

import java.util.Comparator;

// Comparador por peso para el objeto vertice (de mayor a menor).
public class ComparadorPorPeso implements Comparator<Vertice> {

	@Override
	public int compare(Vertice v1, Vertice v2) {
		return -v1.getPeso()+v2.getPeso();
	}

	
}
